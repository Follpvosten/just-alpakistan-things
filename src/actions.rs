use telebot::{bot::RequestHandle, objects::Message};

pub struct ActionRequest {
    pub handle: RequestHandle,
    pub msg: Message,
    pub data: ActionRequestData,
}
pub enum ActionRequestData {
    TwitterAction(TwitterAction),
    DbAction(DbAction),
}
use ActionRequestData as ARD;
impl ActionRequest {
    pub fn twitter(handle: RequestHandle, msg: Message, text: String) -> Self {
        Self {
            handle,
            msg,
            data: ARD::TwitterAction(TwitterAction { text }),
        }
    }
    pub fn db(handle: RequestHandle, msg: Message, request_text: String) -> Option<Self> {
        let data = ARD::DbAction(DbAction::from_context(request_text, &msg.reply_to_message)?);
        Some(Self { handle, msg, data })
    }
}

pub struct ActionResult {
    pub handle: RequestHandle,
    pub orig_msg: Message,
    pub result_text: String,
}

pub struct TwitterAction {
    pub text: String,
}

#[derive(Debug)]
pub enum DbAction {
    Insert {
        category: String,
        title: String,
        text: Option<String>,
    },
    Update {
        title: String,
        change: DbChange,
    },
    Delete {
        title: String,
    },
    Show {
        title: String,
    },
    List {
        category: Option<String>,
    },
}
impl DbAction {
    fn from_context(text: String, orig_msg: &Option<Box<Message>>) -> Option<Self> {
        if let Some(msg) = orig_msg {
            let orig_text = msg.text.as_ref()?;
            let title = orig_text.split('\n').nth(0)?.to_string();
            let result = match text.chars().nth(0)? {
                '#' => {
                    // Attribute editing
                    let change = if let Some(space) = text.find(' ') {
                        let name = text[1..space].to_string();
                        let value = text[space + 1..].to_string();
                        match name.as_ref() {
                            // Basic properties get treated as attributes,
                            // at least for changes
                            "Name" => DbChange::ChangeTitle(value),
                            "Category" => DbChange::ChangeCategory {
                                new_category: value,
                            },
                            _ => DbChange::UpdateAttribute { name, value },
                        }
                    } else if text.starts_with("#-") {
                        let name = text[2..].to_string();
                        DbChange::DeleteAttribute { name }
                    } else {
                        return None;
                    };
                    DbAction::Update { title, change }
                }
                '+' => {
                    // Adding to text
                    let new_text = text[1..].to_string();
                    DbAction::Update {
                        title,
                        change: DbChange::ChangeText(TextChange::Add(new_text)),
                    }
                }
                '-' => {
                    // Removing from text
                    let old_text = text[1..].to_string();
                    DbAction::Update {
                        title,
                        change: DbChange::ChangeText(TextChange::Remove(old_text)),
                    }
                }
                's' => {
                    // Possibly regex replacement
                    let parts = text.split('/').skip(1).collect::<Vec<_>>();
                    if text.starts_with("s/") && parts.len() > 1 {
                        let mut parts = parts.into_iter();
                        let (old_text, new_text) =
                            (parts.next()?.to_string(), parts.next()?.to_string());
                        let global = if let Some(flag) = parts.next() {
                            flag == "g"
                        } else {
                            false
                        };
                        let change = DbChange::ChangeText(TextChange::Replace {
                            old_text,
                            new_text,
                            global,
                        });
                        DbAction::Update { title, change }
                    } else {
                        return None;
                    }
                }
                _ => return None,
            };
            Some(result)
        } else {
            Self::from_string(text)
        }
    }
    fn from_string(text: String) -> Option<Self> {
        if text.starts_with('#') {
            let first_line = text.split('\n').nth(0)?;
            let first_word = first_line.split(' ').nth(0)?;
            let args = &text[first_word.len()..].trim();
            match first_word {
                "#Show" => {
                    if args.is_empty() {
                        None
                    } else {
                        Some(DbAction::Show {
                            title: args.to_string(),
                        })
                    }
                }
                "#List" => Some(DbAction::List {
                    category: if args.is_empty() {
                        None
                    } else {
                        Some(args.to_string())
                    },
                }),
                "#Delete" => {
                    if args.is_empty() {
                        None
                    } else {
                        Some(DbAction::Delete {
                            title: args.to_string(),
                        })
                    }
                }
                _ => {
                    let category = (&first_word[1..]).to_string();
                    let title = (&first_line[first_word.len()..].trim()).to_string();
                    let text = if text.len() > first_line.len() {
                        let second_line_start = text.find('\n').map(|i| i + 1)?;
                        Some((&text[second_line_start..]).to_string())
                    } else {
                        None
                    };
                    Some(DbAction::Insert {
                        category,
                        title,
                        text,
                    })
                }
            }
        } else {
            None
        }
    }
}

#[derive(Debug)]
pub enum DbChange {
    ChangeTitle(String),
    ChangeText(TextChange),
    ChangeCategory { new_category: String },
    UpdateAttribute { name: String, value: String },
    DeleteAttribute { name: String },
}
#[derive(Debug)]
pub enum TextChange {
    Add(String),
    Remove(String),
    Replace {
        old_text: String,
        new_text: String,
        global: bool,
    },
}
