use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Clone)]
pub struct Entry {
    pub title: String,
    pub text: String,
    pub attributes: HashMap<String, String>,
    pub category: String,
}

impl Entry {
    pub fn new<T: Into<String>>(title: T, category: T, text: Option<String>) -> Self {
        Self {
            title: title.into(),
            text: text.unwrap_or_else(String::new),
            attributes: HashMap::new(),
            category: category.into(),
        }
    }
    pub fn attribute(&mut self, name: &str, value: &str) -> Option<String> {
        self.attributes
            .insert(to_capitalized(name.into()), value.into())
    }
    pub fn remove_attribute(&mut self, name: &str) -> Option<String> {
        self.attributes.remove(name)
    }
}

use std::fmt;
impl fmt::Display for &Entry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let text = "\n".to_owned() + self.text.trim();
        let attributes = if self.attributes.is_empty() {
            "".into()
        } else {
            self.attributes
                .iter()
                .map(|(name, value)| format!("{}: {}", name, value))
                .collect::<Vec<_>>()
                .join("\n")
                + "\n"
        };
        write!(
            f,
            "*{}*\nKategorie: {}\n{}{}",
            self.title, self.category, attributes, text
        )
    }
}

fn to_capitalized(input: String) -> String {
    input
        .chars()
        .enumerate()
        .map(|(i, c)| match i {
            0 => c.to_ascii_uppercase(),
            _ => c.to_ascii_lowercase(),
        })
        .collect()
}
