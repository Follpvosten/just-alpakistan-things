use std::{
    fs,
    sync::{Arc, RwLock},
};

use futures::{future::Either, Future, IntoFuture, Stream};
use tokio_core::reactor::Core;

use telebot::{
    bot::{Bot, RequestHandle},
    functions::*,
    objects::*,
};

mod config;
use config::Config;

mod actions;
use actions::*;
mod data;
use data::*;

fn main() {
    println!("Starting bot just-alpakistan-things...");

    println!("Loading config.toml...");
    let bot_config: Config = fs::read_to_string("config.toml")
        .expect("Failed to read config file!")
        .parse()
        .expect("Failed to parse config!");

    println!("Creating Telegram bot...");
    let bot = Bot::new(&bot_config.token).update_interval(200);
    let mut core = Core::new().unwrap();

    println!("Connecting to Twitter...");
    let twitter_token = twitter_bot_token(&bot_config);

    println!("Loading Alpakistan data...");
    let data: Vec<Entry> =
        serde_json::from_str(&fs::read_to_string("data.json").unwrap_or_else(|err| {
            eprintln!("Error loading data.json: {}", err);
            "[]".into()
        }))
        .expect("Failed to parse data.json");
    let db = Arc::new(RwLock::new(data));

    let save_db = || {
        let db_json = {
            let db_arc = Arc::clone(&db);
            let db_read = db_arc.read().unwrap();
            serde_json::to_string(&*db_read)
        }
        .unwrap();
        if let Err(e) = fs::write("data.json", db_json) {
            eprintln!("Failed to write data.json: {}", e);
            eprintln!("Retrying on next change...");
        }
    };

    let action_from_request = |(handle, mut update): (RequestHandle, Update)| {
        let mut message = update.message.take()?;
        let text = message.text.take()?;
        if text.eq_ignore_ascii_case("#JustAlpakistanThings") {
            let orig_message = message.reply_to_message.take()?;
            let text = orig_message
                .text
                .as_ref()
                .unwrap_or(orig_message.caption.as_ref()?)
                .to_owned();
            Some(ActionRequest::twitter(handle, message, text))
        } else {
            ActionRequest::db(handle, message, text)
        }
    };

    loop {
        let stream = bot
            .clone()
            .get_stream(None)
            .filter_map(action_from_request)
            // .filter_map(|(handle, mut update)| {
            //     let mut message = update.message.take()?;
            //     let text = message.text.as_ref()?;
            //     if !text.eq_ignore_ascii_case("#JustAlpakistanThings") {
            //         return None;
            //     }
            //     let orig_message = message.reply_to_message.take()?;
            //     let text = orig_message
            //         .text
            //         .as_ref()
            //         .unwrap_or(orig_message.caption.as_ref()?)
            //         .to_owned();
            //     Some((handle, message, text))
            // })
            // .and_then(|(handle, msg, text, img)| {
            //     if let Some(img) = img {
            //         Either::A(
            //             handle
            //                 .get_file(img[0].file_id.clone())
            //                 .send()
            //                 .and_then(|(_handle, file)| {
            //                     use reqwest::r#async::Client;
            //                     Client::new()
            //                         .get(&format!(
            //                             "https://api.telegram.org/file/bot{}/{}",
            //                             &bot_config.token,
            //                             file.file_path.unwrap()
            //                         ))
            //                         .send()
            //                         .map_err(failure::Error::from)
            //                 })
            //                 .and_then(|mut res| {
            //                     use reqwest::r#async::Decoder;
            //                     use std::mem;
            //                     let body = mem::replace(res.body_mut(), Decoder::empty());
            //                     body.concat2().map_err(failure::Error::from)
            //                 })
            //                 .and_then(move |body| {
            //                     use std::io::{self, Cursor};
            //                     let mut buf: Vec<u8> = vec![];
            //                     let mut body = Cursor::new(body);
            //                     io::copy(&mut body, &mut buf).unwrap();
            //                     Ok((handle, msg, text, Some(buf)))
            //                 }),
            //         )
            //     } else {
            //         use futures::future::result;
            //         Either::B(result(Ok((handle, msg, text, None))))
            //     }
            // })
            .and_then(|ActionRequest { handle, msg, data }| {
                use egg_mode::tweet::DraftTweet;
                use ActionRequestData as ADR;
                match data {
                    ADR::TwitterAction(action) => {
                        let text = action.text.to_owned() + "\n#JustAlpakistanThings";
                        Either::A(
                            DraftTweet::new(text)
                                .send(&twitter_token)
                                .map_err(failure::Error::from)
                                .map(move |_| ActionResult {
                                    handle,
                                    orig_msg: msg,
                                    result_text: "*zwitscher*".into(),
                                }),
                        )
                    }
                    ADR::DbAction(action) => {
                        use futures::future::result;
                        use DbAction::*;
                        let db_arc = Arc::clone(&db);
                        //let dbg_message = format!("Yay! {:#?}", action);
                        let bla = match action {
                            Insert {
                                category,
                                title,
                                text,
                            } => {
                                let db_read = db_arc.read().unwrap();
                                let existing_entry = {
                                    db_read
                                        .iter()
                                        .find(|e| e.title.eq_ignore_ascii_case(&title))
                                };
                                if let Some(entry) = existing_entry {
                                    format!("Entry {} already exists!", entry.title)
                                } else {
                                    // Drop the read-only RwLockGuard so we don't deadlock
                                    drop(db_read);
                                    // Create a new entry
                                    let new_entry = Entry::new(title, category, text);
                                    {
                                        // Get a writable guard
                                        let mut db_write = db_arc.write().unwrap();
                                        // Insert the new entry
                                        db_write.push(new_entry);
                                    }
                                    // Save the changes (deadlocks if there's a write lock)
                                    save_db();
                                    "Added entry successfully!".into()
                                }
                            }
                            Delete { title } => {
                                let mut db_write = db_arc.write().unwrap();
                                if let Some(index) = db_write
                                    .iter()
                                    .position(|e| e.title.eq_ignore_ascii_case(&title))
                                {
                                    db_write.remove(index);
                                    drop(db_write);
                                    // Save the changes
                                    // (deadlocks if there's a write lock on the current thread)
                                    save_db();
                                    "Removed entry successfully!".into()
                                } else {
                                    "Entry not found!".into()
                                }
                            }
                            Show { title } => {
                                let db_read = db_arc.read().unwrap();
                                let entry = db_read
                                    .iter()
                                    .find(|e| e.title.eq_ignore_ascii_case(&title));
                                if let Some(entry) = entry {
                                    format!("{}", entry)
                                } else {
                                    "Entry not found!".into()
                                }
                            }
                            List { category } => {
                                let db_read = db_arc.read().unwrap();
                                if let Some(category) = category {
                                    let entries: Vec<_> = db_read
                                        .iter()
                                        .filter(|e| e.category.eq_ignore_ascii_case(&category))
                                        .collect();
                                    if entries.is_empty() {
                                        "Category not found!".to_string()
                                    } else {
                                        let entries_list = entries
                                            .into_iter()
                                            .map(|e| e.title.clone())
                                            .collect::<Vec<_>>()
                                            .join(",\n");
                                        format!("*{}:*\n{}", category, entries_list)
                                    }
                                } else {
                                    use std::collections::HashMap;
                                    let map = {
                                        let mut map: HashMap<&str, Vec<&str>> = HashMap::new();
                                        for entry in db_read.iter() {
                                            map.entry(&entry.category)
                                                .or_insert_with(Vec::new)
                                                .push(&entry.title);
                                        }
                                        map
                                    };
                                    map.into_iter()
                                        .map(|(key, values)| {
                                            let entries_list = values
                                                .into_iter()
                                                .map(|e| e.to_string())
                                                .collect::<Vec<_>>()
                                                .join(",\n");
                                            format!("*{}:*\n{}", key, entries_list)
                                        })
                                        .collect::<Vec<_>>()
                                        .join("\n\n")
                                }
                            }
                            Update { title, change } => {
                                let mut db_write = db.write().unwrap();
                                if let Some(entry) = db_write
                                    .iter_mut()
                                    .find(|e| e.title.eq_ignore_ascii_case(&title))
                                {
                                    use DbChange::*;
                                    let result = match change {
                                        ChangeTitle(new_title) => {
                                            let old_title = entry.title.split_off(0);
                                            entry.title = new_title;
                                            format!(
                                                "Changed title from *{}* to *{}*!",
                                                old_title, entry.title
                                            )
                                        }
                                        ChangeCategory { new_category } => {
                                            let old_category = entry.category.split_off(0);
                                            entry.category = new_category;
                                            format!(
                                                "Changed category from *{}* to *{}*!",
                                                old_category, entry.category
                                            )
                                        }
                                        UpdateAttribute { name, value } => {
                                            if let Some(old_value) = entry.attribute(&name, &value)
                                            {
                                                format!(
                                                    "Changed attribute *{}*: _{}_ -> _{}_",
                                                    name, old_value, value
                                                )
                                            } else {
                                                format!("Set attribute *{}* to _{}_!", name, value)
                                            }
                                        }
                                        DeleteAttribute { name } => {
                                            entry.remove_attribute(&name);
                                            format!("Deleted attribute {}!", name)
                                        }
                                        ChangeText(change) => {
                                            use TextChange::*;
                                            match change {
                                                Add(text) => entry.text += &text,
                                                Remove(text) => {
                                                    entry.text = entry.text.replace(&text, "")
                                                }
                                                Replace {
                                                    old_text,
                                                    new_text,
                                                    global,
                                                } => {
                                                    if global {
                                                        entry.text = entry
                                                            .text
                                                            .replace(&old_text, &new_text);
                                                    } else {
                                                        entry.text = entry
                                                            .text
                                                            .replacen(&old_text, &new_text, 1);
                                                    }
                                                }
                                            };
                                            format!("Text changed to: {}", entry.text)
                                        }
                                    };
                                    drop(db_write);
                                    save_db();
                                    result
                                } else {
                                    "Unknown entry!".into()
                                }
                            }
                        };
                        Either::B(result(Ok(ActionResult {
                            handle,
                            orig_msg: msg,
                            result_text: bla,
                        })))
                    }
                }
                //let text = text + "\n#JustAlpakistanThings";
                // if let Some(image) = image {
                //     use egg_mode::media::media_types::image_jpg;
                //     use egg_mode::media::UploadBuilder;
                //     Either::A(
                //         UploadBuilder::new(image, image_jpg())
                //             .call(&twitter_token)
                //             .map_err(failure::Error::from)
                //             .and_then(|media_handle| {
                //                 DraftTweet::new(text)
                //                     .media_ids(&[media_handle.id])
                //                     .send(&twitter_token)
                //                     .map_err(failure::Error::from)
                //             })
                //             .map(move |_| (handle, msg)),
                //     )
                // } else {
                //     Either::B(
                // DraftTweet::new(text)
                //     .send(&twitter_token)
                //     .map_err(failure::Error::from)
                //     .map(move |_| (handle, msg))
                //     )
                // }
            })
            .and_then(
                |ActionResult {
                     handle,
                     orig_msg,
                     result_text,
                 }| {
                    handle
                        .message(orig_msg.chat.id, result_text)
                        .parse_mode(ParseMode::Markdown)
                        .reply_to_message_id(orig_msg.message_id)
                        .send()
                },
            );

        println!("Running stream...");
        let res = core.run(stream.for_each(|_| Ok(())).into_future());
        if let Err(e) = res {
            eprintln!("Event loop shutdown:");
            for (i, cause) in e.iter_causes().enumerate() {
                eprintln!(" => {}: {}", i, cause);
            }
        }
    }
}

fn twitter_bot_token(config: &Config) -> egg_mode::Token {
    use egg_mode::{KeyPair, Token};
    let con_token = KeyPair::new(config.consumer_key.clone(), config.consumer_secret.clone());
    let access_token = KeyPair::new(
        config.access_token.clone(),
        config.access_token_secret.clone(),
    );
    Token::Access {
        consumer: con_token,
        access: access_token,
    }
}
